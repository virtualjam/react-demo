# react-demo
A simple react application to showcase react skills

Live demo at https://virtualjam.net/react-demo/

# Screenshots
Default view
![desktop_default](https://virtualjam.net/react-demo/screenshots/hamilton.png)

Comment popup view
![desktop_default](https://virtualjam.net/react-demo/screenshots/hamilton-comment.png)