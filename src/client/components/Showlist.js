import React, { Fragment } from 'react';
import Container from '@material-ui/core/Container';
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";

class DoShowList extends React.Component {
    render() {
        return (
            <Fragment>
                <Container className="List">
                    <Typography> [ Comments ] </Typography>
                    <Container>
                        { this.props.posts.map(function(d, idx) {
                            return (
                                <Container key={idx}>
                                    <Container className="containerStyle">
                                        <Typography component="div" className="circleStyle">{d.author.toUpperCase().charAt(0)}</Typography>
                                        <Typography component="div" className="spacer"/>
                                        <Paper className="listCell">
                                            {decodeURIComponent(d.author)}
                                            <Typography className="smallStyle">{d.timestamp}</Typography>

                                            <Typography component="div" className="spacer"/>
                                            {decodeURIComponent(d.content)}
                                        </Paper>
                                    </Container>
                                </Container>
                            )
                        })}
                    </Container>
                </Container>
            </Fragment>
        )
    }
}
export default DoShowList;