import React, { Component } from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Modal from "@material-ui/core/Modal";
import Typography from "@material-ui/core/Typography";
import { withStyles, createStyles } from "@material-ui/core/styles";
import HamiltonJpg from "../assets/meow.jpg";
import { toggleModal } from "../store/actions";
import DoComments from "./Comment";
import DoShowList from "./Showlist";
import allComments from "../../data/comments";
import './App.css';

class App extends Component {
    static propTypes = {
        classes: PropTypes.object,
        isAddingComment: PropTypes.bool,
        doToggleModal: PropTypes.func
    };
    static defaultProps = {
        isAddingComment: false
    };
    constructor(props) {
        super(props);
        this.state = {
            posts: []
        };
        this.addComment = this.addComment.bind(this);
    }
    componentDidMount() {
        this.getComments();
    }
    addComment(author,comment) {
        allComments.addComment(this, author, comment);
        this.props.doToggleModal();
    }
    getComments() {
        allComments.getComment(this);
    }
    formatTS(ts) {
        return new Intl.DateTimeFormat('en-US', {year: 'numeric', month: '2-digit',day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit'}).format(ts);
    }
    render() {
        const { classes, isAddingComment, doToggleModal } = this.props;
        return (
            <Grid container>
                <Grid item xs={12} className={classes.title}>
                    <Typography gutterBottom variant="h1">
                        Meet Hamilton
                    </Typography>
                    <Paper
                        component="img"
                        src={HamiltonJpg}
                        alt="Hamilton"
                        className={classes.portrait}
                        elevation={4}
                    />
                    <Typography gutterBottom variant="subtitle2">
                        He's a pretty cool cat
                    </Typography>
                    <Button variant="contained" color="primary" onClick={doToggleModal}>
                        Tell me what you think of Hamilton
                    </Button>
                    <Modal open={isAddingComment} onClose={doToggleModal}>
                    <Paper className={classes.commentForm}>
                        <DoComments addComment={this.addComment}/>
                    </Paper>
                    </Modal>
                    <DoShowList posts={this.state.posts}/>
                </Grid>
            </Grid>
        );
    }
}

export default compose(
    withStyles(theme =>
        createStyles({
            title: {
                textAlign: "center"
            },
            portrait: {
                maxWidth: theme.spacing.unit * 48
            },
            commentForm: {
                position: 'absolute',
                width: '50%',
                height: '35%',
                maxWidth: '500px',
                minWidth: '400px',
                minHeight: '250px',
                display: 'flex',
                justifyContent: 'center',
                flexDirection: 'column',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                padding: '20px;',
                boxSizing: 'content-box'
            }
        })
    ),
    connect(
        state => ({
          isAddingComment: state.isAddingComment
        }),
        dispatch => ({
          doToggleModal: () => dispatch(toggleModal())
        }
    )
  )
)(App);