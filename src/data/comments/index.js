const allComments = [];

module.exports = {
    default: allComments,
    getComment(self) {
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if  (this.readyState === 4 && this.status === 200) {
                //console.log("this.responseText "+ this.responseText);
                let result = JSON.parse(this.responseText);
                for ( let i in result ) {
                    if ( result.hasOwnProperty(i) ) {
                        let comment = {author:decodeURIComponent(result[i].author),timestamp:self.formatTS(result[i].timestamp),content:decodeURIComponent(result[i].content)};
                        self.setState(state => {
                            const posts = state.posts.concat(comment);
                            return {
                                posts,
                                comment: '',
                            };
                        });
                    }
                }
            }
        };
        xmlhttp.open("GET", "https://virtualjam.net/react-demo/jasonator.php", true);
        xmlhttp.send();
    },
    addComment(self, author, contents) {
        let comment = {author:decodeURIComponent(author),timestamp:self.formatTS(Date.now()),content:decodeURIComponent(contents)};
        self.setState(state => {
            const posts = state.posts.concat(comment);
            return {
                posts,
                comment: '',
            };
        });
        let xhr = new XMLHttpRequest(),
            jsonRequestURL = "https://virtualjam.net/react-demo/jasonator.php";

        xhr.open("POST", jsonRequestURL, true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function(){
            if  (this.readyState === 4 && this.status === 200) {
                //console.log("this.responseText "+ this.responseText);
            }
        };
        xhr.send("author=" + author + "&timestamp=" + Date.now() + "&content=" + contents );
    }
};
